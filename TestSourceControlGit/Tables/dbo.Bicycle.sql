/*=============================================================


Version:   0.00.0000
Server:    QGJHV6Y2\SQL2019E

DATABASE:	DBCLIBugTwo
  Tables:  Bicycle


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- Create Table [dbo].[Bicycle]
Print 'Create Table [dbo].[Bicycle]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Bicycle] (
		[Bicycle_id]       [int] IDENTITY(1, 1) NOT NULL,
		[Bicycle_name]     [varchar](100) NOT NULL,
		[Bicycle_type]     [varchar](10) NOT NULL,
		CONSTRAINT [PK_b]
		PRIMARY KEY
		CLUSTERED
		([Bicycle_id])
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Bicycle] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

