﻿-- =======================================================
-- Synchronization script for table: [dbo].[Car]
-- =======================================================
Print 'Synchronization script for table: [dbo].[Car]'

SET IDENTITY_INSERT [dbo].[Car] ON
INSERT INTO [dbo].[Car] ([Car_id], [Car_name], [Car_type]) VALUES (1, 'AAAA', 'A')
INSERT INTO [dbo].[Car] ([Car_id], [Car_name], [Car_type]) VALUES (2, 'LLLL', 'L')
INSERT INTO [dbo].[Car] ([Car_id], [Car_name], [Car_type]) VALUES (6, 'GGGG', 'G')
INSERT INTO [dbo].[Car] ([Car_id], [Car_name], [Car_type]) VALUES (10, 'SSSS', 'S')
INSERT INTO [dbo].[Car] ([Car_id], [Car_name], [Car_type]) VALUES (3, 'CCCC', 'C')
INSERT INTO [dbo].[Car] ([Car_id], [Car_name], [Car_type]) VALUES (4, 'DDDD', 'D')
INSERT INTO [dbo].[Car] ([Car_id], [Car_name], [Car_type]) VALUES (5, 'EEEE', 'E')
INSERT INTO [dbo].[Car] ([Car_id], [Car_name], [Car_type]) VALUES (7, 'GGGG', 'G')
INSERT INTO [dbo].[Car] ([Car_id], [Car_name], [Car_type]) VALUES (11, 'WWWW', 'W')
SET IDENTITY_INSERT [dbo].[Car] OFF
