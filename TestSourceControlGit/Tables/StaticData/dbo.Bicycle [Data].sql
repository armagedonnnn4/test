﻿-- =======================================================
-- Synchronization script for table: [dbo].[Bicycle]
-- =======================================================
Print 'Synchronization script for table: [dbo].[Bicycle]'

SET IDENTITY_INSERT [dbo].[Bicycle] ON
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (1, 'AAAA', 'A')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (2, 'BBBB', 'B')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (3, 'CCCC', 'C')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (4, 'DDDD', 'D')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (5, 'EEEE', 'E')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (6, 'FFFF', 'F')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (8, 'HHHH', 'H')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (13, 'OOO', 'O')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (11, '"LLLL"', '"L"')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (12, 'VVVV', 'V')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (9, 'IIII', 'I')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (10, 'GGGG', 'G')
INSERT INTO [dbo].[Bicycle] ([Bicycle_id], [Bicycle_name], [Bicycle_type]) VALUES (14, 'MMMM', 'M')
SET IDENTITY_INSERT [dbo].[Bicycle] OFF
